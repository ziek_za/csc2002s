import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Random;


import java.util.Scanner;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;
//model is separate from the view.

public class WordApp {
//shared variables
	static int noWords=6; //default
	static int totalWords;

   	static int frameX=1000;
	static int frameY=600;
	static int yLimit=480; // past this words will be counted as 'missed'

	static WordDictionary dict = new WordDictionary(); //use default dictionary, to read from file eventually

	static WordRecord[] words;
	static volatile boolean done;  //must be volatile
	static Score score = new Score();

	static WordPanel w;

        //my variables
        static int x_inc;
        static Random r = new Random();
        static String newWord;
        static JLabel scr, missed, caught;
        static final int TEXT_SIZE = 26;
        static boolean gameState = false;
        static int wordCounter = 1;
        static Word[] threads;
        static Thread[] wordThreads;
        static Thread detectionThread;
        static StatsGUI frame;

	public static void main(String[] args) throws InterruptedException {
            //deal with command line arguments
            totalWords = 10;
            //totalWords=Integer.parseInt(args[0]);  //total words to fall
            //noWords=Integer.parseInt(args[1]); //total words falling at any point

            assert(totalWords >= noWords); //this could be done more neatly

            String[] tmpDict = getDictFromFile("example_dict.txt"); //getDictFromFile(args[2]); //file of words

            if (tmpDict != null) {
                    dict = new WordDictionary(tmpDict);
            }

            WordRecord.dict = dict; //set the class dictionary for the words.

            //creats an array of type WordRecord, with the size as the no. of words allowed to fall simultaneously
            words = new WordRecord[noWords];  //shared array of current words
            threads = new Word[noWords];
            wordThreads = new Thread[noWords];
            //Start WordPanel thread - for redrawing animation
            setupGUI(frameX, frameY, yLimit);  
            
            //sets a starting width for each word
            x_inc = (int) frameX/noWords; 
            
            //initialise array
            for (int i = 0; i < noWords; i++) {
                words[i] = new WordRecord();
            }

            //initialise collision detection thread
            detectionThread = new Thread(w);
            detectionThread.start();
            
            while(true) { //10 fps
                Thread.sleep(100);
                w.repaint();
                updateUserGUI();
            }
	}
        
        public static void updateUserGUI() {
            scr.setText("Score: " + score.getScore()+ "    ");
            caught.setText("Caught: " + score.getCaught() + "    ");
	    missed.setText("Missed: " + score.getMissed() + "    ");
        }
        
        public static void wordCollision() {
            score.missedWord();
           // updateUserGUI();
        }
        
        public static boolean endGameCheck() throws InterruptedException {
            if (wordCounter == totalWords) {
                end();
                return true;
            } else {
                wordCounter++;
                return false;
            }
        }
        
        public static void wordEntryCheck(String text) throws InterruptedException {
            for (int i = 0; i < noWords; i++) {
                if (words[i].matchWord(text)) {
                    //update score
                    score.caughtWord(words[i].getWord().length());
                    
                    //reset word
                    if (!endGameCheck()) {
                        words[i].resetWord();
                        words[i].resetSpeed();
                        words[i].resetPos(xPlacement(words[i].getWord()));
                    }
                }
            }
            //updateUserGUI();
        }
	
        public static int xPlacement(String text) {
            int xVal = 0;
            boolean loop = true;
            while (loop) {
                loop = false;
                xVal = r.nextInt(frameX - (text.length()*TEXT_SIZE));
                for (int i = 0; i < noWords; i++) {
                    if (xVal > (words[i].getX() + (words[i].getWord().length() * TEXT_SIZE)) || xVal + (text.length() * TEXT_SIZE) < words[i].getX()) {
                        
                    } else { 
                        loop = true;
                        break;
                    }
                }
            }
            return xVal;
        }
        
        public static void start()  {
            if (!gameState) {
                gameState = true;
                //initialize shared array of current words
                for (int i = 0; i < noWords; i++) {
                        words[i] = new WordRecord(dict.getNewWord() ,i*x_inc, yLimit, TEXT_SIZE, frameX);
                        threads[i] = new Word(words[i]);
                        wordThreads[i] = new Thread(threads[i]);
                        wordThreads[i].start();
                }
            }
        }
        
        public static void end() throws InterruptedException {
            if (gameState) {
                gameState = false;
                //clear the screen and end threads
                for (int i = 0; i < noWords; i++) {
                    //resets words to default
                    words[i] = new WordRecord();
                    //end threads;
                    threads[i].kill();
                    wordThreads[i].join();
                }
                //show stats
                if (score.getTotal() > 0) { //only display if there has been an attempt
                    frame = new StatsGUI(score.getMissed(), score.getCaught(), score.getTotal(), score.getScore());
                    frame.setLocationRelativeTo(null);
                    frame.setVisible(true);
                }
                //reset score
                score.resetScore();
                //reset word counter
                wordCounter = 1;
            }
        }
        
        public static void endAll() throws InterruptedException {
             //end collision detection thread
            w.kill();
            detectionThread.join();
            System.exit(1);
        }
        
	public static void setupGUI(int frameX,int frameY,int yLimit) {
            // Frame init and dimensions
            JFrame frame = new JFrame("PPTDAR001 - WordGame v2.1"); 
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setSize(frameX, frameY);

            JPanel g = new JPanel();
            g.setLayout(new BoxLayout(g, BoxLayout.PAGE_AXIS)); 
            g.setSize(frameX,frameY);
 
            w = new WordPanel(words,yLimit);
            w.setSize(frameX,yLimit+100);
	    g.add(w);
	    	    
	    JPanel txt = new JPanel();
	    txt.setLayout(new BoxLayout(txt, BoxLayout.LINE_AXIS)); 
	    caught =new JLabel("Caught: " + score.getCaught() + "    ");
	    missed =new JLabel("Missed:" + score.getMissed()+ "    ");
	    scr = new JLabel("Score:" + score.getScore()+ "    ");    
	    txt.add(caught);
	    txt.add(missed);
	    txt.add(scr);
  
	    final JTextField textEntry = new JTextField("",20);
            
            textEntry.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent evt) {
                    String text = textEntry.getText();
                    try {
                        wordEntryCheck(text);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(WordApp.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    textEntry.setText("");
                    textEntry.requestFocus();
                }
	    });
	   
            txt.add(textEntry);
            txt.setMaximumSize( txt.getPreferredSize() );
            g.add(txt);
	    
            JPanel b = new JPanel();
            b.setLayout(new BoxLayout(b, BoxLayout.LINE_AXIS)); 
            JButton startB = new JButton("Start");

            // add the listener to the jbutton to handle the "pressed" event
            startB.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e)
                {
                    start();
                    textEntry.requestFocus();  //return focus to the text entry field
                }
            });
            
            JButton endB = new JButton("End");
            
            // add the listener to the jbutton to handle the "pressed" event
            endB.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) { try {
                    //wait until threads stop then end
                    end(); 
                } catch (InterruptedException ex) {
                        Logger.getLogger(WordApp.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });

            JButton quitB = new JButton("Quit");
            // add the listener to the jbutton to handle the "pressed" event
            quitB.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) { try {
                    //wait until threads stop then end
                    end();
                    endAll(); 
                } catch (InterruptedException ex) {
                        Logger.getLogger(WordApp.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            
            b.add(startB);
            b.add(endB);
            b.add(quitB);
            
            g.add(b);
    	
            frame.setLocationRelativeTo(null);  // Center window on screen.
            frame.add(g); //add contents to window
            frame.setContentPane(g);     
            //frame.pack();  // don't do this - packs it into small space
            frame.setVisible(true);
	}
	
        public static String[] getDictFromFile(String filename) {
                String [] dictStr = null;
                try {
                        Scanner dictReader = new Scanner(new File(filename));
                        int dictLength = dictReader.nextInt();
                        System.out.println("read '" + dictLength+"'");

                        dictStr=new String[dictLength];
                        for (int i=0;i<dictLength;i++) {
                                dictStr[i]=new String(dictReader.next());
                                //System.out.println(i+ " read '" + dictStr[i]+"'"); //for checking
                        }
                        dictReader.close();
                } catch (IOException e) {
                System.out.println(e.toString());//System.err.println("Problem reading file " + filename + " default dictionary will be used");
            }
                return dictStr;

        }
}
