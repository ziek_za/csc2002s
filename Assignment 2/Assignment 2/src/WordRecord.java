
import java.util.Random;

public class WordRecord {
    private String text;
    private int x;
    private int y;
    private int maxY;
    private boolean dropped;

    private int fallingSpeed;
    private static int maxWait=100; //1500
    private static int minWait=20; //100

    public static WordDictionary dict;

    //my arguments
    Random r = new Random();
    int TEXT_SIZE, frameX;

    WordRecord() {
            text="";
            x=0;
            y=0;	
            maxY=300;
            dropped=false;
            fallingSpeed=(int)(Math.random() * (maxWait-minWait)+minWait); 
    }

    WordRecord(String text) {
            this();
            this.text=text;
    }

    WordRecord(String text,int x, int maxY, int TEXT_SIZE, int frameX) {
            this(text);
            this.x=x;
            this.maxY=maxY;
            this.TEXT_SIZE = TEXT_SIZE;
            this.frameX = frameX;
    }
	
// all getters and setters must be synchronized
    public synchronized  void setY(int y) {
            if (y>maxY) {
                    y=maxY;
                    dropped=true;
            }
            this.y=y;
    }

    public synchronized  void setX(int x) {
            this.x=x;
    }

    public synchronized  void setWord(String text) {
            this.text=text;
    }

    public synchronized  String getWord() {
            return text;
    }

    public synchronized  int getX() {
            return x;
    }	

    public synchronized  int getY() {
            return y;
    }

    public synchronized  int getSpeed() {
            return fallingSpeed;
    }

    public synchronized void setPos(int x, int y) {
            setY(y);
            setX(x);
    }

    public synchronized void resetPos(int x) {
            setY(0);
            setX(x);
    }

    public synchronized void resetWord() {
            text=dict.getNewWord();
            dropped=false;
            fallingSpeed=(int)(Math.random() * (maxWait-minWait)+minWait); 
            //System.out.println(getWord() + " falling speed = " + getSpeed());

    }
    
    public synchronized void resetSpeed() {
        fallingSpeed = (int)(Math.random() * (maxWait-minWait)+minWait);
    }

    public synchronized boolean matchWord(String typedText) {
            //System.out.println("Matching against: "+text);
            if (typedText.equals(this.text)) {
                    resetWord();
                    return true;
            }
            else
                    return false;
    }

    public synchronized  void drop(int inc) {
            setY(y+inc);
    }

    public synchronized boolean dropped() {
            return dropped;
    }
    
    public synchronized int getMaxY() {
        return maxY;
    }

}
