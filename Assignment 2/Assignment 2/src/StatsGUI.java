import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class StatsGUI extends JFrame {
    private static final int HEIGHT = 165;
    private static final int WIDTH = 250;
    
    public StatsGUI(int missed, int caught, int total, int score) {
        super("End Game Stats");
        setSize(WIDTH, HEIGHT);
        setResizable(false);
        //-----
        String[] labels = {
            "-=-=-=-=-=-",
            "GAME IS OVER",
            "-=-=-=-=-=-",
            "Total words:   " + total,
            "Words caught:   " + caught,
            "Words missed:   " + missed,
            "Percentage caught:   " + ((caught/total) * 100) + "%",
            "Points:   " + score
        };
        //-----
        Box box = Box.createVerticalBox();
        add(box);
        
        for (int i = 0; i < labels.length; i++) {
            JLabel b = new JLabel(labels[i]);
            b.setAlignmentX(JLabel.CENTER_ALIGNMENT);
            box.add(b);
        }
    }
    
}
