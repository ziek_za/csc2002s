public class Word implements Runnable {
    WordRecord word;
    private volatile boolean alive = true;
    
    Word(WordRecord word) {
        this.word = word;
    }
    
    public void kill() {
        alive = false;
    }
        
    @Override
    public void run() {
        while (alive) {
            word.drop(1);
            try {
                Thread.sleep(word.getSpeed());
            } catch (InterruptedException ex) {
            }
        }
    }
}
