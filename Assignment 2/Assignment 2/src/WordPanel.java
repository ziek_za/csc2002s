import java.awt.Color;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JPanel;

public class WordPanel extends JPanel implements Runnable {
        public static volatile boolean running;
        private WordRecord[] words;
        private int noWords;
        private int maxY;

		
        public void paintComponent(Graphics g) {
            int width = getWidth();
            int height = getHeight();
            g.clearRect(0,0,width,height);
            g.setColor(Color.red);
            g.fillRect(0,maxY-10,width,height);

            g.setColor(Color.black);
            g.setFont(new Font("Helvetica", Font.PLAIN, 26));
           //draw the words
           //animation must be added 
            for (int i = 0; i < noWords; i++){	    	
                g.drawString(words[i].getWord(),words[i].getX(),words[i].getY());	
            }
        }

        WordPanel(WordRecord[] words, int maxY) {
                this.words=words; //will this work?
                noWords = words.length;
                //threads = new Thread[noWords];
                running=true;
                this.maxY=maxY;		
        }

        public void kill() {
            running = false;
        }
        
        @Override
        public void run() {
            while (running) { //game is running
                
                for (int i = 0; i < noWords; i++) {
                    if(words[i].getY() >= words[i].getMaxY()) {
                        try {
                            if (!WordApp.endGameCheck()) {
                                WordApp.wordCollision();
                                words[i].resetWord();
                                words[i].resetSpeed();
                                words[i].resetPos(WordApp.xPlacement(words[i].getWord()));
                            }
                        } catch (InterruptedException ex) {
                            Logger.getLogger(WordPanel.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
        }

}


