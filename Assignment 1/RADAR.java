import java.io.*;
import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

class RADAR {
	//ForkJoin Pool, global object
	static final ForkJoinPool fjPool = new ForkJoinPool();

	//Timing methods and arguments
	static long startTime = 0;
	
	private static void tick(){
		startTime = System.currentTimeMillis();
	}
	private static float toc(){
		return (System.currentTimeMillis() - startTime) / 1000.0f; 
	}

	public static void main(String [] args) throws IOException {
		//Arguments
		final String RECEIVE = args[0] + ".txt";
		final String TRANSMIT = args[1] + ".txt";
		float[] receivedArray, transmittedArray, correlatedArray, test;
		final int ARRAY_SIZE;
		final int LOOP_ITERATIONS = 7;
		int receivedArraySize, transmittedArraySize;

		//Reading in transmitted and received values
		Scanner receivedScanner = new Scanner(new File(RECEIVE));
		Scanner transmittedScanner = new Scanner(new File(TRANSMIT));
		
		//Check for largest array size from received and transmitted data sets
		receivedArraySize = receivedScanner.nextInt();
		transmittedArraySize = transmittedScanner.nextInt();
		if (receivedArraySize != transmittedArraySize) {
			if (receivedArraySize > transmittedArraySize) {
				ARRAY_SIZE = receivedArraySize;
			} else {
				ARRAY_SIZE = transmittedArraySize;
			}
		} else {
			ARRAY_SIZE = receivedArraySize;
		}

		//initializing data set arrays of type float
		receivedArray = new float[ARRAY_SIZE];
		transmittedArray = new float[ARRAY_SIZE];
		correlatedArray = new float[ARRAY_SIZE];

		//populating arrays with inputed data sets
		for (int i = 0; i < ARRAY_SIZE; i++) {
			receivedArray[i] =receivedScanner.nextFloat();
			transmittedArray[i] = transmittedScanner.nextFloat();
		} //for
		
		//--USED FOR PLOTTING GRAPHS
		//PointPlot k = new PointPlot(receivedArray, "ReceivedArray-Graph");
		//PointPlot j = new PointPlot(transmittedArray, "TransmittedArray-Graph");
		
		//k.plotPoints();
		//j.plotPoints();
		//--
		
		//Arguments
		Scanner computeMethod = new Scanner(System.in);
		String computeMethodInput;
		
		System.out.println("DETECTED\n------------\nFiles in: " + RECEIVE + " & " + TRANSMIT + "\nArray Size: " + ARRAY_SIZE + "\n-------------");
		System.out.println("Select a computing method:\nSequential (s) OR Parallel (p)");
		
		computeMethodInput = computeMethod.nextLine();
		
		
		if (computeMethodInput.equalsIgnoreCase("s")) {
			System.out.println("Sequential Cross Correlation and Max Value");
			sequentialLoop(LOOP_ITERATIONS, ARRAY_SIZE, receivedArray, transmittedArray);
		} else if (computeMethodInput.equalsIgnoreCase("p")) {
			System.out.println("Parallel Cross Correlation and Max Value");
			parallelLoop(LOOP_ITERATIONS, ARRAY_SIZE, receivedArray, transmittedArray);
		} else {
			System.out.println("Invalid input");
		}

		
		//--PLOT CORRELATED ARRAY
		//PointPlot p = new PointPlot(correlatedArray, "Correlated-Graph");
		//p.plotPoints();
		//--
} //main

	static void sequentialLoop(int iteration, int ARRAY_SIZE, float[] receivedArray, float[] transmittedArray) {
		//arguments
		float correlationTotTime = 0;
		float correlationTime = 0;
		float maxValTotTime = 0;
		float maxValTime = 0;
		float maximumValue = 0;
		float[] arr;
		
		System.out.println(ARRAY_SIZE + " points\n--------------------");
		System.out.println("Iteration - Correlation & Max");
		
		for (int i = 0; i < iteration; i++) {
			tick();
			arr = sequentialCorrelate(receivedArray, transmittedArray);
			correlationTime = toc();
			
			tick();
			maximumValue = maxValue(arr);
			maxValTime = toc();
			
			correlationTotTime += correlationTime;
			maxValTotTime += maxValTime;
			
			System.out.println("[" + (i + 1) + "] - " + correlationTime +"s & " + maxValTime + "s");
		} //for
		System.out.println("--------------------\nAveraged time: " + correlationTotTime/iteration + "s & " + maxValTotTime/iteration + "s");
		System.out.println("Maximum value: " + maximumValue);

		
	} //seqeuntialLoop
	
	static void parallelLoop(int iteration, int ARRAY_SIZE, float[] receivedArray, float[] transmittedArray) {
		//arguments
		float correlationTotTime = 0;
		float correlationTime = 0;
		float maxValTotTime = 0;
		float maxValTime = 0;
		float maximumValue = 0;
		float[] arr;
		
		System.out.println(ARRAY_SIZE + " points\n--------------------");
		System.out.println("Iteration - Correlation & Max");
		
		for (int i = 0; i < iteration; i++) {
			tick();
			arr = parallelCorrelate(receivedArray, transmittedArray, ARRAY_SIZE);
			correlationTime = toc();
			
			tick();
			maximumValue = forkJoinMaxValue(arr);
			maxValTime = toc();
			
			correlationTotTime += correlationTime;
			maxValTotTime += maxValTime;
		
			System.out.println("[" + (i + 1) + "] - " + correlationTime +"s & " + maxValTime + "s");
		} //for
		System.out.println("--------------------\nAveraged time: " + correlationTotTime/iteration + "s & " + maxValTotTime/iteration + "s");
		System.out.println("Maximum value: " + maximumValue);
		
	} //parallelLoop

	static float[] parallelCorrelate(float[] rec, float[] trans, int ARRAY_SIZE) {
		//Arguments
		float[] ansArray = new float[ARRAY_SIZE];
		
		for (int i = 0; i < ARRAY_SIZE; i++) {
			ansArray[i] = fjPool.invoke(new CorrelateArrays(rec, trans, 0, rec.length, i));
		}
		return ansArray;

	} //parallelCorrelate

	static float maxValue(float[] arr) {
		//arguments
		float max = 0;

		for (int i = 0; i < arr.length; i++) {
			if (i == 0) {
				max = arr[i];
			} else if (arr[i] > max) {
				max = arr[i];
			}
		} //for

		return max;
	} //maxValue

	static float forkJoinMaxValue(float[] arr) {
		return fjPool.invoke(new MaxValue(arr, 0, arr.length));
	}

	static float[] sequentialCorrelate(float[] receivedArray, float[] transmittedArray) {
		//The code below has been sourced from the assignment brief.
		
		//Arguments
		float sum;
		float[] ansArray = new float[receivedArray.length];

		for (int i = 0; i < receivedArray.length; ++i) {
			sum = 0;
			for (int j = 0; j < transmittedArray.length; ++j) {
				if (i+j < receivedArray.length) {
					sum += receivedArray[i+j] * transmittedArray[j];
				} //if
			} //for
			ansArray[i] = sum;
		} //for

		return ansArray;
	} //sequentialCorrelat

} //RADAR
