+++++++++++++++++++++++++++++++++++++++++++
+ Author: Darryn Papathanasiou, PPTDAR001 +
+ Date: 08 August 2014                    +
+++++++++++++++++++++++++++++++++++++++++++

Name: Digital Signal Processing with the Java Fork/Join framework.

Description: 	Given a received and transmitted data set, it will compute the correlated data set and give the max value,
		using sequential or parallel methods.

Aside:		Given is the code to plot the graphs using JFreeChart however I was not able to insert the .jar libraries for JFreeChart
		into the build path outside of Eclipse.

Instructions: 
1. Ensure Java 6 is installed.
2. Navigate to where the source files in the command line.
3. Enter 'make clean' and 'make' into terminal window.
4. Ensure your files for correlation are inside the directory of the source files.
5a. Inside the command line, enter 'java RADAR <recievedFileName> <transmittedFileName>' without the file extension, it assumes it is in .txt.
5b. Inside the command line, enter 'make run' where it is defaulted to receive.txt and transmit.txt.
6. Select the method you wish to correlate the data with with Sequential (s) and Parallel (p).

List of files: 
	> JAVA: RADAR.java CorrelateArrays.java MaxValue.java PointPlot.java
	> OTHER: README.txt(this), Makefile, receive.txt, transmit.txt (100 000 points), jfreechart.jar, jcommon.jar

/*FIN*/