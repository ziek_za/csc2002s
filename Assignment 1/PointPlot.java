import java.io.*;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.chart.ChartFactory; 
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.JFreeChart; 
import org.jfree.chart.ChartUtilities;

public class PointPlot {
	float[] arr;
	String fileName;
	int xVal = 0;
	
	public PointPlot(float[] arr, String fileName) {
		this.arr = arr;
		this.fileName = fileName;
	}

	public void plotPoints() {
        try {
			DefaultCategoryDataset line_chart_dataset = new DefaultCategoryDataset();
			for (int i = 0; i < arr.length; i++) {
				line_chart_dataset.addValue(arr[i], fileName,"" + i);
			}
          
			JFreeChart lineChartObject=ChartFactory.createLineChart(fileName,"Time","Signal Value",line_chart_dataset,PlotOrientation.VERTICAL,true,true,false);                
            
			int width=940; /* Width of the image */
			int height=580; /* Height of the image */                
			File lineChart=new File(fileName + ".png");              
			ChartUtilities.saveChartAsPNG(lineChart,lineChartObject,width,height); 
        }
        catch (Exception i)
        {
            System.out.println(i);
        }
	}
} //PointPlot
