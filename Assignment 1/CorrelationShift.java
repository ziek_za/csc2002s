import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;
import java.util.Arrays;

class CorrelationShift extends RecursiveTask<float[]> {
	float[] receivedArray, transmittedArray, correlatedArray, leftAns, rightAns;
	static final int SEQUENTIAL_CUTOFF = 500;
	int low, hi, index;
	final int ARRAY_SIZE;
	float ans;
	static final ForkJoinPool fjPool = new ForkJoinPool();


	public CorrelationShift(float[] receivedArray, float[] transmittedArray, int low, int hi, int ARRAY_SIZE) {
		this.receivedArray = receivedArray;
		this.transmittedArray = transmittedArray;
		this.low = low;
		this.hi = hi;
		this.index = index;
		this.ARRAY_SIZE = ARRAY_SIZE;
		correlatedArray = new float[ARRAY_SIZE];
	}

	public float[] compute() {
		if ((hi - low) < SEQUENTIAL_CUTOFF) {
			for (int i = low; i < hi; i++) {
				correlatedArray[i] = fjPool.invoke(new CorrelateArrays(receivedArray, transmittedArray, low, hi, i));
			} //for
			return correlatedArray;

		} else {
			CorrelationShift left = new CorrelationShift(receivedArray, transmittedArray, low, (low+hi)/2, ARRAY_SIZE);
			CorrelationShift right = new CorrelationShift(receivedArray, transmittedArray, (low+hi)/2, hi, ARRAY_SIZE);
			left.fork();
			rightAns = right.compute();
			leftAns  = left.join(); 
			return merge(leftAns, rightAns);
		}
	}

	static float[] merge(float[] A, float[] B) {
		float[] mergedArr = new float[A.length];
		
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < A.length; j++) {
				if (i == 0) {
					mergedArr[j] = A[j];
				} else {
					mergedArr[j] = B[j];
				}
			}
			 
		} //for
		return mergedArr;
	} //merge

} //CorrelateArrays
