import java.io.*;
import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

class Main {
	//In files
	static final String RECEIVE = "receive.txt";
	static final String TRANSMIT = "transmit.txt";
	static final ForkJoinPool fjPool = new ForkJoinPool();

	static long startTime = 0;
	
	private static void tick(){
		startTime = System.currentTimeMillis();
	}
	private static float toc(){
		return (System.currentTimeMillis() - startTime) / 1000.0f; 
	}

	public static void main(String [] args) throws IOException {
		//Arguments
		float[] receivedArray, transmittedArray, correlatedArray, test;
		final int ARRAY_SIZE;
		int receivedArraySize, transmittedArraySize;
		boolean firstEl = true;

		//Reading in transmitted and received values
		Scanner receivedScanner = new Scanner(new File(RECEIVE));
		Scanner transmittedScanner = new Scanner(new File(TRANSMIT));
		
		//Check for largest array size from received and transmitted data sets
		receivedArraySize = receivedScanner.nextInt();
		transmittedArraySize = transmittedScanner.nextInt();
		if (receivedArraySize != transmittedArraySize) {
			if (receivedArraySize > transmittedArraySize) {
				ARRAY_SIZE = receivedArraySize;
			} else {
				ARRAY_SIZE = transmittedArraySize;
			}
		} else {
			ARRAY_SIZE = receivedArraySize;
		}

		//initializing data set arrays of type float
		receivedArray = new float[ARRAY_SIZE];
		transmittedArray = new float[ARRAY_SIZE];
		correlatedArray = new float[ARRAY_SIZE];

		//populating arrays with inputed data sets
		for (int i = 0; i < ARRAY_SIZE; i++) {
			receivedArray[i] =receivedScanner.nextFloat();
			transmittedArray[i] = transmittedScanner.nextFloat();
		} //for

		
		//Parallel fork/join correlation compute
		tick();
		correlatedArray = parallelCorrelate(receivedArray, transmittedArray, ARRAY_SIZE);
		float time = toc();
		
		//sequential correlate
		tick();		
		test = sequentialCorrelate(receivedArray, transmittedArray);
		float time2 = toc();

		System.out.println("Parallel: " + time);
		System.out.println("Sequential: " + time2);
		//Max value
		//System.out.println(maxValue(test));
		//System.out.println(forkJoinMaxValue(correlatedArray));

	} //main

	static float[] parallelCorrelate(float[] rec, float[] trans, int ARRAY_SIZE) {
		//Arguments
		float[] ansArray = new float[ARRAY_SIZE];
		for (int i = 0; i < ARRAY_SIZE; i++) {
			ansArray[i] = fjPool.invoke(new CorrelateArrays(rec, trans, 0, rec.length, i));
		}
		return ansArray;

	} //parallelCorrelate

	static float maxValue(float[] arr) {
		//arguments
		float max = 0;

		for (int i = 0; i < arr.length; i++) {
			if (i == 0) {
				max = arr[i];
			} else if (arr[i] > max) {
				max = arr[i];
			}
		} //for

		return max;
	} //maxValue

	static float forkJoinMaxValue(float[] arr) {
		return fjPool.invoke(new MaxValue(arr, 0, arr.length));
	}

	static float[] sequentialCorrelate(float[] receivedArray, float[] transmittedArray) {
		//Arguments
		float sum;
		float[] ansArray = new float[receivedArray.length];

		for (int i = 0; i < receivedArray.length; ++i) {
			sum = 0;
			for (int j = 0; j < transmittedArray.length; ++j) {
				if (i+j < receivedArray.length) {
					sum += receivedArray[i+j] * transmittedArray[j];
				} //if
			} //for
			ansArray[i] = sum;
		} //for

		return ansArray;
	} //sequentialCorrelat

} //RADAR